import { Formik , Form , Field } from "formik"
import { useNavigate } from "react-router-dom"
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import * as Yup from 'yup'
import Alerta from './Alerta'
import BeatLoader from "react-spinners/BeatLoader"

const Formulario = ({cliente,cargando}) => {

  const navigate = useNavigate()

  const MySwal =  withReactContent(Swal)

  const nuevoClienteScheme =  Yup.object().shape({
            nombre: Yup.string()
                       .min(4, 'El nombre es muy corto.')
                       .max(50, 'El nombre es muy largo')
                       .required('El nombre del cliente es necesario.'),

            empresa: Yup.string()
                        .required('El nombre de la empresa es necesario.'),

            email: Yup.string()
                      .email('Correo no valido')
                      .required('El correo del cliente es necesario.'),

            telefono: Yup.number()
                         .integer('Debe ser un número valido')
                         .positive('Teléfono no valido')
                         .typeError('Debe ser un número telefonico valido'),

            notas: Yup.string(),
  })

  const handleSubmit = async (values) => {

        let respuesta

        if (cliente.id) {

            let {id} = cliente

            const url = `http://localhost:4000/clientes/${id}`

            respuesta = await fetch( url , {
                method: 'PUT',
                body: JSON.stringify(values),
                headers: {
                    'Content-type' : 'application/json'
                }
            })
        } else {

            const url = 'http://localhost:4000/clientes'

            respuesta = await fetch( url , {
                method: 'POST',
                body: JSON.stringify(values),
                headers: {
                    'Content-type' : 'application/json'
                }
            })
    
        }


  }

  return (
    <div>
        { cargando ? <BeatLoader className="text-center mt-10" color={'blue'} loading={cargando} size={20}/> : (
            <div className='mt-3 rounded-md md:w-3/4 mx-auto
                bg-white px-5 py-10 shadow-lg'> 

                <h2 className='font-semibold text-center uppercase'> 
                    Datos cliente 
                </h2> 

                <Formik
                    initialValues={{
                        nombre: cliente?.nombre ?? "", 
                        empresa: cliente?.empresa ?? "",
                        email: cliente?.email ?? "",
                        telefono: cliente?.telefono ?? "",
                        notas: cliente?.notas ?? "",
                    }}

                    enableReinitialize={true}

                    onSubmit = { async (values, {resetForm}) => {
                        handleSubmit(values)

                        MySwal.fire({
                            title: cliente?.id  ? 'Editado' : 'Agregado' ,
                            text: cliente?.id  ? 'El cliente fue editado satisfactoriamente' : 'El cliente fue agregado satisfactoriamente',
                            icon: 'success',
                            showConfirmButton: true,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                          }).then((result) => {
                            if (result.isConfirmed) {
                                resetForm()
                                navigate("/clientes")
                            }
                        })
                    }}
                    
                    validationSchema={nuevoClienteScheme}
                >
                
                {({errors,touched}) =>  (
                    <Form>
                        <div className='mt-4 mb-4'>
                            <label className='text-gray-800 ' htmlFor='nombre'> Nombre: </label>
                            <Field
                             id='nombre'
                             name = 'nombre'
                             className='block bg-gray-100 w-full p-2 rounded mt-2'
                             type='text'
                             placeholder='Escriba el nombre del cliente'
                            />
                            { errors.nombre && touched.nombre ? <Alerta>{errors.nombre}</Alerta> : null}
                        </div>
                        <div className='mt-4 mb-4'>
                            <label className='text-gray-800' htmlFor='empresa'> Empresa del cliente: </label>
                            <Field
                             id='empresa'
                             name = 'empresa'
                             className='block bg-gray-100 w-full p-2 rounded mt-2'
                             type='text'

                             placeholder='Escriba la empresa del cliente'
                            />
                            { errors.empresa && touched.empresa ? <Alerta>{errors.empresa}</Alerta> : null}
                        </div>
                        <div className='mt-4 mb-4'>
                            <label className='text-gray-800' htmlFor='email'> Email: </label>
                            <Field
                             id='email'
                             name = 'email'
                             className='block bg-gray-100 w-full p-2 rounded mt-2'
                             type='email'
                             placeholder='Escriba el email del cliente'
                            />
                            { errors.email && touched.email ? <Alerta>{errors.email}</Alerta> : null}
                        </div>
                        <div className='mt-4 mb-4'>
                            <label className='text-gray-800' htmlFor='telefono'> Teléfono: </label>
                            <Field
                             id='telefono'
                             name = 'telefono'
                             className='block bg-gray-100 w-full p-2 rounded mt-2'
                             type='tel'
                             placeholder='Escriba el teléfono del cliente'
                            />
                            { errors.telefono && touched.telefono ? <Alerta>{errors.telefono}</Alerta> : null}                            
                        </div>
                        <div className='mt-4 mb-4'>
                            <label className='text-gray-800' htmlFor='notas'> Notas: </label>
                            <Field
                             as='textarea'
                             id='notas'
                             name = 'notas'
                             className='block bg-gray-100 w-full p-2 rounded mt-2 h-30'
                             type='text'
                             placeholder='Escriba alguna nota del cliente'
                            />
                            { errors.notas && touched.notas ? <Alerta>{errors.notas}</Alerta> : null}   
                        </div>

                        <input
                            type='submit'
                            className='bg-blue-900 uppercase p-3 block w-full text-white text-center font-medium'
                            value={ cliente?.nombre ? 'Editar cliente' : 'Agregar cliente'}
                        />
                    </Form>

                )}
                </Formik>

            </div>
        )}
    </div>
  )

  Formulario.defaultProps  = {
    cliente : {},
    cargando: false
  }
}

export default Formulario