
const Alerta = ({children}) => {
  return (
    <div className="w-full bg-red-500 text-white text-center p-2 uppercase mt-2 mb-2 rounded">
        {children}
    </div>
  )
}

export default Alerta