import { useNavigate} from 'react-router-dom'

const Cliente = ({cliente,handleEliminar}) => {

  const navigate = useNavigate()
  const {nombre,empresa,telefono,email,notas,id} = cliente

  return (
   <>
   <tr className='hover:bg-gray-100'>
        <td className='p-3 border border-blue-100'>{nombre}</td>
        <td className='p-3 border border-blue-100'>
            <p className='text-blue-800 font-bold uppercase'> 
                Email: 
                <span className='text-black font-normal normal-case'>
                 {''} {email}
                </span>
            </p>
            <p className='text-blue-800 font-bold uppercase'> 
                Teléfono: 
                <span className='text-black font-normal normal-case'>
                 {''} {telefono}
                </span>
            </p>
        </td>
        <td className='p-3 border border-blue-100'> {empresa}</td>
        <td className='p-3 border border-blue-100'>{notas}</td>
        <td className='p-3 border border-blue-100'>
            <button className='w-full block bg-blue-500 uppercase
                              text-white rounded p-2 font-bold hover:bg-blue-300'
                    onClick={ () => navigate(`${id}`)}>
                Ver
            </button>
            <button className='w-full block bg-blue-700 uppercase
                              text-white rounded p-2 mt-1 font-bold hover:bg-blue-600'
                    onClick={ () => navigate(`editar/${id}`)}>
                Editar
            </button>
            <button className='w-full block bg-blue-800 uppercase
                              text-white rounded p-2 mt-1 font-bold hover:bg-blue-900'
                    onClick={ () => handleEliminar(id)}>
                Eliminar
            </button>
        </td>
    </tr>
   </> 
  )
}

export default Cliente