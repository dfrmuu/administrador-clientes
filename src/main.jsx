import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter , Routes , Route } from "react-router-dom";  
import Layout from './layout/Layout';
import {ClientIndex , EditClient , NewClient, SeeClient } from "./pages/clients/Clients";
import './index.css' 

ReactDOM.createRoot(document.getElementById('root')).render(
  <BrowserRouter>
        <Routes>
          <Route path='/' element = {<Layout/>}>
             <Route index element={<ClientIndex/>}/>
          </Route>


          <Route path='/clientes' element={<Layout/>}>
              <Route index element={<ClientIndex/>}/>
              <Route path='nuevo' element = {<NewClient/>}/>
              <Route path='editar/:id' element = {<EditClient/>}/>
              <Route path=':id' element = {<SeeClient/>}/>
          </Route>
        </Routes>
  </BrowserRouter>
)
