import {useState,useEffect} from 'react'
import { useParams } from "react-router-dom";
import Formulario from '../../components/Formulario'

const EditClient = () => {

  const {id} = useParams()
  const [cliente,setCliente] = useState({})
  const [cargando, setCargando] = useState(false)

  useEffect(() => {
        try{
            setCargando(true)
            const obtenerClienteAPI = async () => {
                const url = `http://localhost:4000/clientes/${id}`
                const respuesta = await fetch(url)
                const resultado = await respuesta.json()

                setCliente(resultado)

                setTimeout(() => {
                   setCargando(false)  //Detiene el spinner
                }, 1000);
            }

            obtenerClienteAPI()
        }catch(error){
            console.log(error)
        }
  }, [])
  

  return (
    <div>
      <h1 className='text-indigo-500 text-4xl font-black'>
        Editar cliente
      </h1>
      <h2 className='font-medium mt-3'>
         Realice los cambios y presione editar:
      </h2>
      <Formulario 
        cliente={cliente}
        cargando={cargando}/>
    </div>
  )
}

export default EditClient