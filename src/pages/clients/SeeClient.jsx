import { useEffect, useState } from "react"
import { useParams } from 'react-router-dom'
import BeatLoader from "react-spinners/BeatLoader";

const SeeClient = () => {

  const {id} = useParams()
  const [cliente,setCliente] = useState({})
  const [cargando, setCargando] = useState(false)

  useEffect(() => {
        try{
            setCargando(true)
            const obtenerClienteAPI = async () => {
                const url = `http://localhost:4000/clientes/${id}`
                const respuesta = await fetch(url)
                const resultado = await respuesta.json()

                setCliente(resultado)

                setTimeout(() => {
                   setCargando(false)  //Detiene el spinner
                }, 1000);
            }

            obtenerClienteAPI()
        }catch(error){
            console.log(error)
        }
  }, [])
  

  return (

        <>

        { cargando == true ? <BeatLoader className="text-center mt-10" color={'blue'} loading={cargando} size={20}/> : (  

        <div>
                <h1 className='text-indigo-500 text-4xl font-black'>
                        Consulta del cliente ({cliente.nombre})
                </h1>

                <div className="bg-white p-10 mt-10 shadow-lg">
                        <div>
                                <p className="text-2xl">
                                <span className="font-bold text-3xl text-indigo-500">Nombre:</span> 
                                {' '}
                                {cliente.nombre}
                                </p>
                        </div>

                        <div className='mt-4'>
                                <p className="text-2xl">
                                <span className="font-bold text-3xl text-indigo-500">Empresa:</span> 
                                {' '}
                                {cliente.empresa}
                                </p>
                        </div>

                        <div className='mt-4'>
                                <p className="text-2xl">
                                <span className="font-bold text-3xl text-indigo-500">Teléfono:</span> 
                                {' '}
                                {cliente.telefono}
                                </p>
                        </div>


                        <div className='mt-4'>
                                <p className="text-2xl">
                                <span className="font-bold text-3xl text-indigo-500">Email:</span> 
                                {' '}
                                {cliente.email}
                                </p>
                        </div>

                        <div className='mt-4'>
                                <p className="text-2xl">
                                <span className="font-bold text-3xl text-indigo-500">Notas:</span> 
                                {' '}
                                {cliente.notas}
                                </p>
                        </div>
                </div>
        </div>    
        )}

        </>
  )
}

export default SeeClient