import React from 'react'
import Formulario from '../../components/Formulario'

const NewClient = () => {

  const cliente = {}

  return (
    <div>
      <h1 className='text-indigo-500 text-4xl font-black'>
        Nuevo cliente
      </h1>
      <h2 className='font-medium mt-3'>
         Digite los datos del cliente a registrar:
      </h2>
      <Formulario
        cliente={cliente}/>
    </div>
  )
}

export default NewClient