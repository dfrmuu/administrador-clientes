import { useState, useEffect } from "react";
import Cliente from "../../components/Cliente"
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

const ClientIndex = () => {

  const [clientes,setClientes] = useState([])

  const mySwal = withReactContent(Swal)

  const handleEliminar =  id => {
    mySwal.fire({
      title: 'Deseas eliminar este cliente?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
          const eliminarClienteAPI = async id => {
            const url = `http://localhost:4000/clientes/${id}`
            const respuesta = await fetch(url, {
              method: 'DELETE'
            })

            const arrayClientes = clientes.filter( cliente => cliente.id !== id)

            setClientes(arrayClientes)
          }

          eliminarClienteAPI(id)
      }
    })
  }

  useEffect(() => {

      try {
        
          const obtenerClientesAPI = async () => {
              const url = 'http://localhost:4000/clientes'
              const respuesta = await fetch(url)
              const resultado = await respuesta.json()
              setClientes(resultado)
          }     

          obtenerClientesAPI()

      } catch (error) {
        console.log(error)
      } 

  }, [])
  

  return (
    <div>
      <h1 className='text-indigo-500 text-4xl font-black'>
        Lista clientes
      </h1>
      <h2 className='font-medium mt-3'>
         Aqui la lista de clientes:
      </h2>

       <table className="table w-full table-auto shadow-md bg-white mt-5 border-collapse border">
          <thead className="bg-blue-800 text-white">
              <tr>
                <th className="p-2"> Nombre </th>
                <th className="p-2"> Empresa </th>
                <th className="p-2"> Email / Teléfono </th>
                <th className="p-2"> Notas </th>
                <th className="p-2"> Acciones</th>
              </tr>
          </thead>
          
          <tbody>
              {clientes.map( cliente => (
                  <Cliente
                    key={cliente.id}
                    cliente = {cliente}
                    handleEliminar = {handleEliminar}/>
              ))}
          </tbody>
       </table>
    </div>
  )
}

export default ClientIndex